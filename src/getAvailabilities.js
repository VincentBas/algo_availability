import moment from 'moment'
import knex from 'knexClient'
import {rangeDate, groupBy} from './generic'

/**
* @desc Build Appointement object with starts_at and ends_at
* @return <Object> 
*/
const toAppointment = (...args) => toOpening(...args);

/**
* @desc Build Opening object with starts_at and ends_at
* @return <Object> 
*/
const toOpening = (start, end) => {
  return {
    starts_at : start,
    ends_at : end
  };
};

const filterAppointment = (appointment, slots) => {
  const appointments = rangeDate(appointment.starts_at, appointment.ends_at);
  return slots.filter(slot => !(appointments.find(x => slot.isSame(x))));
};

const intoCurrentWeek = (day, opening) => {
  const { starts_at, ends_at } = opening;
  while (starts_at < day) {
    starts_at.add(7, 'days');
    ends_at.add(7, 'days');
  }
  return toOpening(moment(starts_at), moment(ends_at));
};

/**
* @desc Formatted slot HH:mm by date
* @return <Array> of object with date and slots
*/
const availabilitiesFormatted = (date, slots) => {
  const weekSlots = rangeDate(date, date.clone().add(7, 'days'), 1, 'days');
  return weekSlots.map(datetime => {
    const day = moment.utc(datetime).startOf('days');
    const slotsDay = slots[String(moment.utc(datetime))] || [];
    return {
      date: String(new Date(datetime)),
      slots: slotsDay.map(date =>{
        const newDate = new Date(date)
        const hour = Number.parseInt(newDate.getHours()).toString()
        const minutes = (newDate.getMinutes() < 10 ? '0' : '') + newDate.getMinutes()
        return `${hour}:${minutes}`
      })
    };
  })
};

/**
* @desc Collect Opening event
* @return <Array> of opening event
*/
const getOpenings = async (date) => {
  const dateAfter7Days = moment(date.toDate()).add(7, 'days');
  const reccuringOpening = await knex('events')
        .select('starts_at', 'ends_at')
        .where({kind: 'opening', weekly_recurring: true})
        .andWhere('starts_at', '<=', dateAfter7Days.valueOf());
  const weeklyOpening = await knex('events')
        .select('starts_at', 'ends_at')
        .where({kind: 'opening', weekly_recurring: false})
        .andWhere('starts_at', '>=', date.valueOf())
        .andWhere('ends_at', '<=', dateAfter7Days.valueOf());
  return [...reccuringOpening, ...weeklyOpening]
  .map(({starts_at, ends_at}) => toOpening(moment(starts_at), moment(ends_at)))
  .map(opening => intoCurrentWeek(date, opening));
};

/**
* @desc Collect Appointement event
* @return <Array> of appointement event
*/
const getAppointments = async (date) => {
  const dateAfter7Days = moment(date.toDate()).add(7, 'days');
  return knex('events')
    .select('starts_at', 'ends_at')
    .where({kind: 'appointment'})
    .andWhere('starts_at', '>=', date.valueOf())
    .andWhere('ends_at', '<=', dateAfter7Days.valueOf())
    .then(
      appointments => appointments
        .map(({starts_at, ends_at}) => toAppointment(moment(starts_at), moment(ends_at))));
};

/**
* @desc Return all availabilities for a date
* @return <Array> of object with date and slots
*/
async function getAvailabilities(date) {
  const currentDate = moment(date);
  const openings = await getOpenings(currentDate);
  const appointments = await getAppointments(currentDate);
  const openingSlots = openings
        .reduce((acc, {starts_at, ends_at}) => [...acc, ...rangeDate(starts_at, ends_at)],[]);
  const availableSlots = appointments
        .reduce((acc, appointment) => filterAppointment(appointment, acc), openingSlots);
  const groupByDate = groupBy(availableSlots, (date) => moment.utc(date.clone()).startOf('days'));
  return availabilitiesFormatted(currentDate, groupByDate);
}

module.exports = {
  filterAppointment,
  intoCurrentWeek,
  availabilitiesFormatted,
  getOpenings,
  getAppointments,
  getAvailabilities,
};