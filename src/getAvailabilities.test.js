import knex from 'knexClient'
import moment from 'moment'
import { factory } from './factory'
import {
  filterAppointment,
  intoCurrentWeek,
  getAvailabilities, 
  getOpenings, 
  getAppointments, 
  availabilitiesFormatted
} from './getAvailabilities'

describe('getAvailabilities functions', () => {
  beforeEach(() => knex('events').truncate())

  describe('intoCurrentWeek()', () => {
    it('should not modify an opening if it is on the same week', () => {
      const day = factory.date('2014-08-11');
      const reccuring_opening = {
        starts_at: factory.date('2014-08-12 09:30'),
        ends_at: factory.date('2014-08-12 12:30')
      };
      expect(intoCurrentWeek(day, reccuring_opening)).toEqual(reccuring_opening);
    });
  })

  describe('getOpenings', async () => {
    it('should get recurring opening where starts_at < arg_date', async () => {
      const opening = factory.opening({ weekly_recurring: true });
      await knex('events').insert(opening);
      const openingDates = await getOpenings(factory.date('2014-08-10'));
      expect(openingDates.length).toBe(1);
    });

    it('should not get recurring opening where starts_date > arg_date', async () => {
      const opening = factory.opening({
        starts_at: new Date('2014-08-20 09:30'),
        ends_at: new Date('2014-08-20 12:30'),
        weekly_recurring: true
      });
      await knex('events').insert(opening);
      const openingDates = await getOpenings(factory.date('2014-08-10'));
      expect(openingDates.length).toBe(0);
    });

    it('should not get opening where the date is already passed', async () => {
      const opening = factory.opening({ weekly_recurring: false });
      await knex('events').insert(opening);
      const openingDates = await getOpenings(factory.date('2014-08-10'));
      expect(openingDates.length).toBe(0);
    });

    it('should get opening where the date is the week', async () => {
      const opening = factory.opening({
        starts_at: new Date('2014-08-11 09:30'),
        ends_at: new Date('2014-08-11 12:30'),
        weekly_recurring: false
      });
      await knex('events').insert(opening);
      const openingDates = await getOpenings(factory.date('2014-08-10'));
      expect(openingDates.length).toBe(1);
    });

    it('should not get appointment', async () => {
      const opening = factory.opening({ kind: 'appointment' });
      await knex('events').insert(opening);
      const openingDates = await getOpenings(factory.date('2014-08-10'));
      expect(openingDates.length).toBe(0);
    });
  })

  describe('getAppointments', async () => {
    it('should not get openings', async () => {
      const opening = factory.opening();
      await knex('events').insert(opening);
      const appointment = await getAppointments(factory.date('2014-08-10'));
      expect(appointment.length).toBe(0)
    });
    it('should not get past or futur appointment', async () => {
      const pastAppointment = factory.appointment({
        starts_at: new Date('2014-08-06 11:30'),
        ends_at: new Date('2014-08-06 12:30'),
      });
      const futurAppointment = factory.appointment({
        starts_at: new Date('2014-08-19 8:30'),
        ends_at: new Date('2014-08-19 10:00'),
      });
      await knex('events').insert([pastAppointment, futurAppointment]);
      const appointment = await getAppointments(factory.date('2014-08-11'));
      expect(appointment.length).toBe(0)
    });
    it('should get week appointment', async () => {
      const weekAppointment = factory.appointment({
        starts_at: new Date('2014-08-11 10:30'),
        ends_at: new Date('2014-08-11 12:00'),
      });
      await knex('events').insert(weekAppointment);
      const appointment = await getAppointments(factory.date('2014-08-10'));
      expect(appointment.length).toBe(1)
    });
  })

  describe('getAvailabilities', () => {
    beforeEach(async () => {
      await knex('events').insert([
        {
          kind: 'opening',
          starts_at: new Date('2014-08-04 09:30'),
          ends_at: new Date('2014-08-04 12:30'),
          weekly_recurring: true,
        },
        {
          kind: 'appointment',
          starts_at: new Date('2014-08-11 10:30'),
          ends_at: new Date('2014-08-11 11:30'),
        },
      ])
    })

    it('should fetch availabilities correctly', async () => {
      
      const availabilities = await getAvailabilities(new Date('2014-08-10'));
      expect(availabilities.length).toBe(7)

      expect(String(availabilities[0].date)).toBe(
        String(new Date('2014-08-10')),
      )

      expect(availabilities[0].slots).toEqual([])

      expect(String(availabilities[1].date)).toBe(
        String(new Date('2014-08-11')),
      )
      expect(availabilities[1].slots).toEqual([
        '9:30',
        '10:00',
        '11:30',
        '12:00',
      ])

      expect(availabilities[2].slots).toEqual([])

      expect(String(availabilities[6].date)).toBe(
        String(new Date('2014-08-16')),
      )
    })
  })
})
