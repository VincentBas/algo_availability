import moment from 'moment'

/**
* @desc Build model of event kind object and date
* @return <Object>
*/
const factory = {

    opening: (args = {}) => {
      return Object.assign({
        kind: 'opening',
        starts_at: new Date('2014-08-04 09:30'),
        ends_at: new Date('2014-08-04 12:30'),
        weekly_recurring: true
      }, args);
    },
  
    appointment: (args = {}) => {
      return Object.assign({
        kind: 'appointment',
        starts_at: new Date('2014-08-10 09:30'),
        ends_at: new Date('2014-08-10 12:30')
      }, args);
    },
  
    date: (date) => {
      return moment(new Date(date));
    },
  }

  module.exports = {
    factory
  };