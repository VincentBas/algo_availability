import moment from 'moment';
import { factory } from './factory';
import { rangeDate, groupBy } from './generic';

describe('generic functions', () => {

  describe('rangeDate', () => {
    it('should generate empty array if the interval is invalid', () => {
      const start = factory.date('2014-08-10 12:30');
      const end = factory.date('2014-08-10 09:30');
      expect(rangeDate(start, end)).toEqual([]);
    });

    it('should generate a list of slots between two date with 30 min interval by default', () => {
      const start = factory.date('2014-08-10 09:30');
      const end = factory.date('2014-08-10 12:30');
      expect(rangeDate(start, end)).toEqual([
        factory.date('2014-08-10 09:30'),
        factory.date('2014-08-10 10:00'),
        factory.date('2014-08-10 10:30'),
        factory.date('2014-08-10 11:00'),
        factory.date('2014-08-10 11:30'),
        factory.date('2014-08-10 12:00'),
      ]);
    });
  });

  describe('groupBy', () => {

    it('should group by odd or not odd', () => {
      const numbers = [1, 2, 3, 4, 5, 6];
      expect(groupBy(numbers, (n) => n % 2)).toEqual({
        0: [2, 4 ,6],
        1: [1, 3, 5]
      });
    });

    it('should group datetime by day', () => {
      const datetimes = [
        factory.date('2014-08-10 09:30'),
        factory.date('2014-08-10 10:00'),
        factory.date('2014-08-11 11:00'),
        factory.date('2014-08-12 11:30'),
        factory.date('2014-08-12 12:00'),
      ];
      const groupByDate = groupBy(datetimes, (date) => date.clone().startOf('days'));
      expect(groupByDate).toEqual({
        [factory.date('2014-08-10').startOf('days')]: [
          factory.date('2014-08-10 09:30'),
          factory.date('2014-08-10 10:00'),
        ],
        [factory.date('2014-08-11').startOf('days')]: [
          factory.date('2014-08-11 11:00'),
        ],
        [factory.date('2014-08-12').startOf('days')]: [
          factory.date('2014-08-12 11:30'),
          factory.date('2014-08-12 12:00'),
        ]
      });
    });
  });
});
