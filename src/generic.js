import moment from 'moment'

/**
* @desc Collect all slots between start and end date
* @return <Array> of time slot
*/
const rangeDate = (start, end, step=30, unit='minutes') => {
  let slots = []
  if(start > end) return slots;
  let iterateDate = start.clone()
  while(iterateDate < end){
    slots = [...slots, moment(iterateDate.toDate())];
    iterateDate.add(step, unit);
  }
  return slots;
}

/**
* @desc Sort element according to a condition
* @return <Object> of arrays
*/
const groupBy = (list, func) => {
  return list.reduce((acc, value) => {
    const key = func(value);
    const oldValue = acc[key] || [];
    return Object.assign(acc, {[key]: [ ...oldValue, value]});
  }, {}); 
}

module.exports = {
  rangeDate,
  groupBy
};
  